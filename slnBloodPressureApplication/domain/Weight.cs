﻿using System;

namespace BP.Domain
{
    public class Weight : BaseDataObject
    {

        public Weight()
        {
            
        }

        public Weight(DateTime readingDate, double pounds)
        {
            DateRecorded = readingDate;
            Pounds = pounds;

        }

        public virtual DateTime DateRecorded { get; set; }
        public virtual double Pounds { get; set; }
    }
}