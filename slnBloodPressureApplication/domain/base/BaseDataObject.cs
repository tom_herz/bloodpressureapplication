﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BP.Domain.Annotations;

namespace BP.Domain
{
    public abstract class BaseDataObject : IBaseDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual DateTime? CreatedOn { get; set; }
        public virtual DateTime? LastModifiedOn { get; set; }
        public virtual bool IsDeleted { get; set; }
       
    }
}