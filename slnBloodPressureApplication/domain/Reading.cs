﻿using System;
using BP.Common;
using common;

namespace BP.Domain
{
    public class Reading : BaseDataObject
    {
        private double _diastolic;

        private readonly double _highReading = Convert.ToDouble(Constants.Reading_High);
        private readonly double _lowReading = Convert.ToDouble(Constants.Reading_Low);
         

        /// <summary>
        /// The date of a blood pressure reading.
        /// </summary>
        public virtual  DateTime ReadingDate { get; set; }

        /// <summary>
        /// The top number of a blood pressure reading.
        /// </summary>
        public virtual  double Systolic { get; set; }

        /// <summary>
        /// The bottom number of a blood pressure reading.
        /// </summary>
        public virtual  double Diastolic
        {
            get
            {
                AssignRating();

                return _diastolic;
            }
            set
            {
                _diastolic = value;
            }
        }

        public virtual  Rating Rating { get; set; }

        public virtual  Weight Weight { get; set; }
 
        public virtual ReadingLocation ReadingLocation { get; set; }

        #region private domain logic
        
        private void AssignRating()
        {
            if (_diastolic > _highReading)
                Rating = Rating.High;

            if (_diastolic >= _lowReading && _diastolic <= _highReading)
                Rating = Rating.Normal;

            if (_diastolic < _lowReading)
                Rating = Rating.Low;
        }

        #endregion

    }
}
