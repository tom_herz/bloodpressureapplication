﻿using BP.Common;

namespace BP.Domain
{
    public class ContactInformation : BaseDataObject
    {
        public virtual string EmailAddress { get; set; }
        public virtual EmailAddressType EmailAddressType { get; set; }
    }
}