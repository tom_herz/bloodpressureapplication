using System;

namespace BP.Domain
{
    public interface IBaseDataObject
    {
        Guid Id { get; set; }
      
        DateTime? CreatedOn { get; set; }
        
        DateTime? LastModifiedOn { get; set; }

        bool IsDeleted { get; set; }
    }
}