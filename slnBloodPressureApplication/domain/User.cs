﻿using System;
using System.Collections.Generic;

namespace BP.Domain
{
    public  class User : BaseDataObject
    {
        public virtual String First { get; set; }
        public virtual String Last { get; set; }
        public virtual IList<ContactInformation> ContactInformation { get; set; }
        public virtual IList<Reading> Readings { get; set; }
    }
}
