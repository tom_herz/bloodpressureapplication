﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using BP.Domain;
using BP.Domain.Annotations;
using BP.Interfaces;
using BP.Service;


namespace BP.UI.WPF
{
    public class BloodPressureViewModel : INotifyPropertyChanged
    {
        private double _txtSystolic;
        private double _txtDiastolic;
        private DateTime _txtReadingDate;

        protected IReadingService ReadingService { private get; set; }

        public Reading Reading { get; set; }

   
        public BloodPressureViewModel()
        {
            if (ReadingService == null)
                ReadingService = new ReadingService();
           

            Reading = new Reading()
                {
                    Systolic = TxtSystolic,
                    Diastolic = TxtDiastolic,
                    ReadingDate = TxtReadingDate
                };
        }

        public ICommand ButtonCommand
        {
            get
            {
                return new DelegateCommand(AddReading);
            }
        }

        private void AddReading()
        {
                ReadingService.SaveReading(Reading);
        }

        public double TxtSystolic
        {
            get { return _txtSystolic; }
            set
            {
                _txtSystolic = Convert.ToDouble(value);
                OnPropertyChanged();
            }
        }

        public double TxtDiastolic
        {
            get { return _txtDiastolic; }
            set
            {
                _txtDiastolic = Convert.ToDouble(value);
                OnPropertyChanged();
            }
        }

        public DateTime TxtReadingDate
        {
            get { return _txtReadingDate; }
            set
            {
                _txtReadingDate = value;
                OnPropertyChanged();
            }
        }

        public static string[] ConfigLocations
        {
            get
            {
                return new String[]
                    {
                        "config://spring/objects"
                    };
            }


        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
