﻿using System.Windows;
using BP.Interfaces;
using BP.Service;


namespace BP.UI.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        protected IReadingService ReadingService { private get; set; }

      
        public MainWindow()
        {
        
            InitializeComponent();
        }
    }
}
