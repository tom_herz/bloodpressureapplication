﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BP.Domain;
using BP.Interfaces;
using BP.Service;

namespace BP.UI.MVC.Controllers
{
    public class DataSummaryController : Controller
    {
        //
        // GET: /DataSummary/

        protected IReadingService ReadingService { private get; set; }

       

        public ViewResult Index()
        {
            var readings = ReadingService.GetAllReadings();
            
            var _presentation = new List<ReadingPresentation>();

            
            foreach (var reading in readings)
            {
                
                _presentation.Add(new ReadingPresentation(reading));
            }

            return View(_presentation.ToList());

        }

    }
}
