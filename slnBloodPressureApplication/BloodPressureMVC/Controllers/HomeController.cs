﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using BP.Data;
using BP.Data.queries;
using BP.Domain;
using Microsoft.Ajax.Utilities;

namespace BP.UI.MVC.Controllers
{
    public class HomeController : Controller
    {
        protected IUserDao UserDao;

        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult UserList()
        {
            var users = UserDao.FindAll(new QueryForAllUsers()) as IList<User>;

            return View(users);
        }
 
        public ActionResult UserDetail(string id)
        {
            var userId = Guid.Parse(id);
            
            var user = UserDao.GetById(userId);

            return View(user);

        }
    }
}
