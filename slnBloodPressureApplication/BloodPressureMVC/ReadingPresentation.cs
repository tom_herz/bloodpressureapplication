﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using BP.Domain;
using common;

namespace BP.UI.MVC
{
    public class ReadingPresentation
    {

        public ReadingPresentation(Reading reading)
        {
            ReadingDate = reading.ReadingDate.ToString();
            Systolic = reading.Systolic.ToString();
            Diastolic = reading.Diastolic.ToString();
            Weight = reading.Weight == null ? null : reading.Weight.Pounds.ToString(CultureInfo.InvariantCulture);
            ReadingLocation = reading.ReadingLocation.ToString();
            Rating = reading.Rating.ToString();

        }

        public string Rating { get; set; }
        public string ReadingDate { get; set; }
        public string Systolic { get; set; }
        public string Diastolic { get; set; }
        public string Weight { get; set; }
        public string ReadingLocation { get; set; }
    }
}