﻿using System.Collections.Generic;
using BP.Domain;

namespace BP.Presentation
{
    public class UserPresentation
    {
        public UserPresentation()
        {
            
        }

        public UserPresentation(User user)
        {
            FirstName = user.First;
            LastName = user.Last;
            EmailAddress = GetPrimaryEmailAddress(user.ContactInformation) ?? null;
       
        }

        public string EmailAddress { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

    

        private string GetPrimaryEmailAddress(IList<ContactInformation> contactInformation)
        {
            string result = contactInformation[0].EmailAddress;
            
            return result;

        }
    }
}
