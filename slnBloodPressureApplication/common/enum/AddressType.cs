namespace BP.Common
{
    public enum AddressType
    {
        Main = 0,
        Personal = 1,
        Work = 2,
        Primary = 3,
        Secondary = 4
    }
}