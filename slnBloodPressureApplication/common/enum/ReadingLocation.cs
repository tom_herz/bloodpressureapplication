﻿namespace BP.Common
{
    public enum ReadingLocation
    {
        RightArm = 0,
        LeftArm,
        RightAnkle,
        LeftAngle
    }
}