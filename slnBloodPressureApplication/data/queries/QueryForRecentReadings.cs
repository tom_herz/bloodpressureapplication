﻿using BP.Domain;
using NHibernate;

namespace BP.Data.queries
{
    public class QueryForRecentReadings:IAbstractQuery<Reading>
    {
        public int NumberToTake { get; set; }
        public IQueryOver<Reading> BuildQueryOver(ISession session)
        {
            return session.QueryOver<Reading>().OrderBy(x => x.ReadingDate).Desc.Take(NumberToTake);
        }

        public IQueryCriteria BuildQuery(ISession session)
        {
            throw new System.NotImplementedException();
        }
    }
}