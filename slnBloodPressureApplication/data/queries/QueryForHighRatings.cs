﻿using System;
using BP.Domain;
using NHibernate;
using common;

namespace BP.Data.queries
{
    public class QueryForHighRatings : IAbstractQuery<Reading>
    {
        public IQueryOver<Reading> BuildQueryOver(ISession session)
        {
            return session.QueryOver<Reading>().Where(x => x.Rating == Rating.High);
        }

        public IQueryCriteria BuildQuery(ISession session)
        {
            throw new NotImplementedException();
        }
    }
}