﻿using System;
using BP.Domain;
using NHibernate;

namespace BP.Data.queries
{
    public class QueryAllReadingsByUserId : IAbstractQuery<User>
    {
        public Guid UserId { get; set; }

        public IQueryOver<User> BuildQueryOver(ISession session)
        {
            return session.QueryOver<User>().Where(x => x.Id == UserId).Fetch(x => x.Readings).Eager;
        }

        public IQueryCriteria BuildQuery(ISession session)
        {
            throw new NotImplementedException();
        }
    }
}