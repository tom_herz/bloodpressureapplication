using BP.Domain;
using NHibernate;

namespace BP.Data.queries
{
    public class QueryAllWeightRecords : IAbstractQuery<Weight>
    {
        public IQueryOver<Weight> BuildQueryOver(ISession session)
        {
            return session.QueryOver<Weight>();
        }

        public IQueryCriteria BuildQuery(ISession session)
        {
            throw new System.NotImplementedException();
        }
    }
}