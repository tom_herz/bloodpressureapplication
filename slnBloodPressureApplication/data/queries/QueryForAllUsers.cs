﻿using BP.Domain;
using NHibernate;
using NHibernate.Linq;

namespace BP.Data.queries
{
    public class QueryForAllUsers : IAbstractQuery<User>
    {
        public IQueryOver<User> BuildQueryOver(ISession session)
        {
            return session.QueryOver<User>();   
        }

        public IQueryCriteria BuildQuery(ISession session)
        {
            throw new System.NotImplementedException();
        }
    }
}