﻿using System;
using BP.Domain;
using NHibernate;
using common;


namespace BP.Data.queries
{
    public class QueryForReadingsByRatingAndDateRange:IAbstractQuery<Reading>
    {
        public Rating Rating { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }


        public IQueryOver<Reading> BuildQueryOver(ISession session)
        {
            return
                session.QueryOver<Reading>()
                       .WhereRestrictionOn(x => x.ReadingDate)
                       .IsBetween(Start)
                       .And(End)
                       .And(x => x.Rating == Rating);
        }

        public IQueryCriteria BuildQuery(ISession session)
        {
            throw new NotImplementedException();
        }
    }
}