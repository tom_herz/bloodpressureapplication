﻿using System;
using BP.Domain;
using NHibernate;
using NHibernate.Impl;

namespace BP.Data.queries
{
    public class QueryAllReadings:IAbstractQuery<Reading>
    {
        public IQueryOver<Reading> BuildQueryOver(ISession session)
        {
            return session.QueryOver<Reading>();
        }

        public IQueryCriteria BuildQuery(ISession session)
        {
            throw new NotImplementedException();
        }
    
    }
}