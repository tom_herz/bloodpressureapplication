﻿using System;
using BP.Domain;
using NHibernate;
using common;

namespace BP.Data.queries
{
    public class QueryNormalReadings : IAbstractQuery<Reading>
    {
        public IQueryOver<Reading> BuildQueryOver(ISession session)
        {
            return session.QueryOver<Reading>().Where(x => x.Rating == Rating.Normal);
        }

        public IQueryCriteria BuildQuery(ISession session)
        {
            throw new NotImplementedException();
        }
    }
}