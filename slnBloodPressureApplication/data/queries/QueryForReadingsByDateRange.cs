﻿using System;
using BP.Domain;
using NHibernate;

namespace BP.Data.queries
{
    public class QueryForReadingsByDateRange:IAbstractQuery<Reading>
    {


        public DateTime Start { get; set; }
        public DateTime End { get; set; }


        public IQueryOver<Reading> BuildQueryOver(ISession session)
        {
            return session.QueryOver<Reading>().WhereRestrictionOn(x => x.ReadingDate).IsBetween(Start).And(End);
        }

        public IQueryCriteria BuildQuery(ISession session)
        {
            throw new NotImplementedException();
        }

    }
}