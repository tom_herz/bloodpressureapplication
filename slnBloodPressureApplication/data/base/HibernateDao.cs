﻿using System;
using System.Collections.Generic;
using BP.Domain;

using NHibernate;
using Spring.Transaction;
using Spring.Transaction.Interceptor;


namespace BP.Data
{
    public abstract class HibernateDao<TEntity, TId> : IDisposable, ICanQuery<TEntity>, IRepository<TEntity, TId> where TEntity : IBaseDataObject
    {
      
        /// <summary>
        /// Spring favors property injection, but other DI containers (like Autofac) better support ctor injection
        /// ctor injection is much more intention revealing about dependencies - nonetheless no-op ctor for spring
        /// </summary>
        protected HibernateDao(/*ISession session*/)
        {
            //this._session = session;
        }


        private ISessionFactory _sessionFactory;

        /// <summary>
        /// Session factory for sub-classes.
        /// </summary>
        public ISessionFactory SessionFactory
        {
            protected get { return _sessionFactory; }
            set { _sessionFactory = value; }
        }

        /// <summary>
        /// Get's the current active session. Will retrieve session as managed by the 
        /// Open Session In View module if enabled.
        /// </summary>
        protected ISession CurrentSession
        {
            get { return _sessionFactory.GetCurrentSession(); }
        }


        #region Impl of IDisposable

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _sessionFactory.Dispose();
                }
            }
            _disposed = true;
        }


        #endregion


        #region Impl of ICanQuery

        [Transaction(ReadOnly = true)]
        public TEntity FindOne(IAbstractQuery<TEntity> abstractQuery)
        {
            return abstractQuery.BuildQueryOver(CurrentSession).SingleOrDefault();
        }

        [Transaction(ReadOnly = true)]
        public IEnumerable<TEntity> FindAll(IAbstractQuery<TEntity> abstractQuery)
        {
            return abstractQuery.BuildQueryOver(CurrentSession).List();
        }

        [Transaction(ReadOnly = true)]
        public IList<TEntity> PagedQuery(IAbstractQuery<TEntity> abstractQuery, int start, int numItems)
        {
            return abstractQuery.BuildQueryOver(CurrentSession).Skip(start).Take(numItems).List();
        }

        [Transaction(ReadOnly = true)]
        public int RecordsFound(IAbstractQuery<TEntity> abstractQuery)
        {
            return abstractQuery.BuildQueryOver(CurrentSession).List().Count;
        }

        #endregion


        #region Impl of IRepository

        [Transaction(ReadOnly = true)]
        public TEntity GetById(TId id)
        {
            try
            {
                return CurrentSession.Get<TEntity>(id);
            }
            catch (Exception)
            {

        //        Logger.Warn(String.Format(@"An attempt was made to access an object than no longer exists. Id:  {0}", id));
                return default(TEntity);
            }
        }

        [Transaction(TransactionPropagation.Required)]
        public TId Add(TEntity entity)
        {
            return (TId)CurrentSession.Save(entity);
        }

        [Transaction(TransactionPropagation.Required)]
        public void Update(TEntity entity)
        {
            entity.LastModifiedOn = DateTime.Now;
            CurrentSession.Update(entity);
        }

        [Transaction(TransactionPropagation.Required)]
        public void Remove(TEntity entity)
        {
            entity.IsDeleted = true;
            entity.LastModifiedOn = DateTime.Now;

            CurrentSession.Update(entity);
        }

        [Transaction()]
        public void Delete(TEntity entity)
        {
            try
            {
                CurrentSession.Delete(entity);
            }
            catch (Exception e)
            {

        //        Logger.Warn(e.Message);
            }

        }

        #endregion

    }
}
