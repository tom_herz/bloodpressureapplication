﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Cfg;
using Spring.Data.NHibernate;

namespace BP.Data
{
     public class FluentNHibernateSessionFactory : LocalSessionFactoryObject
    {
        /// <summary>
        /// Sets the assemblies to load that contain fluent nhibernate mappings.
        /// </summary>
        /// <value>The mapping assemblies.</value>
        public string[] FluentNhibernateMappingAssemblies
        {
            get;
            set;
        }

         protected override void PostProcessConfiguration(global::NHibernate.Cfg.Configuration config)
        {

            base.PostProcessConfiguration(config);
            
            Fluently.Configure(config)
                .Mappings(m =>
                              {
                                  if (FluentNhibernateMappingAssemblies != null)
                                  {
                                      foreach (string fluentAssembly in FluentNhibernateMappingAssemblies)
                                      {
                                          Assembly a = Assembly.Load(fluentAssembly);

                                          if (a == null)
                                              throw new ArgumentException(
                                                  string.Format("The assembly referenced by {0} is invalid.",
                                                                fluentAssembly));

                                          m.HbmMappings.AddFromAssembly(a);

                                          m.FluentMappings/*.Conventions.Setup(x => x.Add(AutoImport.Never()))*/
                                              .AddFromAssembly(a);
                                      }
                                  }

                              })
                .BuildConfiguration();
         

            config.SetProperties(ConvertPropsFromSpring());
     //     config.SetInterceptor(new SqlInterceptor());

        }

       Dictionary<string, string> ConvertPropsFromSpring() {
           return HibernateProperties.ToDictionary(de => de.Key, de => de.Value);
       }
    }
}
