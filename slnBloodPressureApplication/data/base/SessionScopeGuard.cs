﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spring.Data.NHibernate.Support;

namespace BP.Data
{
    /// <summary>
    /// The SessionScopeGuard will guard the session and close it when the guard is disposed.
    /// </summary>
    public class SessionScopeGuard : IDisposable
    {
        private OpenSessionInViewModule osiv;

        /// <summary>
        /// A convenience constructor initializing the session scope guard with
        /// an instanciated open session in view.
        /// </summary>
        public SessionScopeGuard()
        {
            osiv = new OpenSessionInViewModule();

            osiv.Open();
        }

        /// <summary>
        /// Dispose the open session.
        /// </summary>
        public void Dispose()
        {
            osiv.Close();
        }

    }
}
