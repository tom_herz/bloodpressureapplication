using BP.Domain;
using NHibernate;

namespace BP.Data
{
    public interface IAbstractQuery<TEntity> where TEntity : IBaseDataObject
    {
        IQueryOver<TEntity> BuildQueryOver(ISession session);
        
        IQueryCriteria BuildQuery(ISession session);
    }
}