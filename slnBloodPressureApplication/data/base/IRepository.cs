using BP.Domain;

namespace BP.Data
{
    public interface IRepository<T, TId> where T : IBaseDataObject
    {

        T GetById(TId id);
    
        TId Add(T entity);
        
        void Update(T entity);
        
        void Remove(T entity);
        
        void Delete(T entity);

    }
}
