using System;
using BP.Domain;

namespace BP.Data
{
    public interface IReadingDao :  ICanQuery<Reading>,IRepository<Reading, Guid>
    {
    }
}