using System;
using BP.Domain;

namespace BP.Data
{
    public interface IWeightDao: ICanQuery<Weight>, IRepository<Weight,Guid>
    {
    }
}