﻿using System;
using BP.Domain;

namespace BP.Data
{
    public interface IUserDao: ICanQuery<User>, IRepository<User, Guid>
    {
    }
}