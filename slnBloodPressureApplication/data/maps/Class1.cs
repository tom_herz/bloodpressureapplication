﻿using BP.Common;
using BP.Domain;
using FluentNHibernate.Mapping;
using common;

namespace BP.Data
{
    public class ContactInformationMap : ClassMap<ContactInformation>
    {


        public ContactInformationMap()
        {
            Table("ContactInformation");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Guid().Column("Id");
            Map(x => x.EmailAddress);
            Map(x => x.EmailAddressType).CustomType<EmailAddressType>();
        }
    }
}
