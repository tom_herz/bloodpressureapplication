﻿using BP.Domain;
using FluentNHibernate.Mapping;

namespace BP.Data
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("tblUser");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Guid().Column("Id");

            Map(x => x.First);
            Map(x => x.Last);
            HasMany(x => x.ContactInformation).Cascade.All();
            HasMany(x => x.Readings).Cascade.All();

        }
    }
}