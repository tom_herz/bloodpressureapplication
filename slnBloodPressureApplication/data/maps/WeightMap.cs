﻿using BP.Domain;
using FluentNHibernate.Mapping;

namespace BP.Data
{
    public class WeightMap : ClassMap<Weight>
    {
        public WeightMap()
        {
            Table("Weight");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Guid().Column("Id");
            Map(x => x.DateRecorded);
            Map(x => x.Pounds);


        }
    }
}