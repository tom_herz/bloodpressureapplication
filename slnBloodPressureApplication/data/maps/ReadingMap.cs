﻿using BP.Domain;
using FluentNHibernate.Mapping;
using BP.Common;
using common;

namespace BP.Data
{
    public class ReadingMap : ClassMap<Reading>
    {
        public ReadingMap()
        {
            Table("Reading");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Guid().Column("Id");

            Map(x => x.ReadingDate).Column("Date");
            Map(x => x.Systolic).Column("Systolic");
            Map(x => x.Diastolic).Column("Diastolic");
            Map(x => x.Rating).CustomType<Rating>().Default("1");
            Map(x => x.ReadingLocation).CustomType<ReadingLocation>().Default("0");
            References(x => x.Weight).Cascade.All();

        }

    }

   
}