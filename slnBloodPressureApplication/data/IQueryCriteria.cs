﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using common;

namespace BP.Data
{
 public   interface IQueryCriteria
    {
     DateTime? StartDate { get; set; }
     DateTime? EndDate { get; set; }
     Rating? Rating { get; set; }   
 }

    class QueryCriteria : IQueryCriteria
    {
        private Rating? _rating;

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Rating? Rating
        {
            get {
                return _rating ?? null;
            }
            set { _rating = value; }
        }
    }
}
