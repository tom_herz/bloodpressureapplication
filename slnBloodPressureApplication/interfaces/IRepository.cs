﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BP.Interfaces
{
    public interface IRepository<T, TId> where T : IBaseDataObject
    {

        T GetById(TId id);
    
        TId Add(T entity);
        
        void Update(T entity);
        
        void Remove(T entity);
        
        void DeleteRecord(T entity);

    }
}
