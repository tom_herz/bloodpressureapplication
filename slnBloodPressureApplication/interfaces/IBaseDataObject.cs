using System;

namespace BP.Interfaces
{
    public interface IBaseDataObject
    {
        Guid Id { get; set; }
      
        DateTime? CreatedOn { get; set; }
        
        DateTime? LastModifiedOn { get; set; }
    }
}