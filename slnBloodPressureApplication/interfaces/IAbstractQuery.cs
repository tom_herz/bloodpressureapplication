﻿using NHibernate;

namespace BP.Interfaces
{
    public interface IAbstractQuery<TEntity> where TEntity : IBaseDataObject
    {
        IQueryOver<TEntity> BuildQueryOver(ISession session);
        
        ICriteria BuildQuery(ISession session);
    }
}