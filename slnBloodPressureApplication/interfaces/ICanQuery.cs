﻿using System.Collections.Generic;

namespace BP.Interfaces
{
   public interface ICanQuery<TEntity> where TEntity : IBaseDataObject 
    {
        TEntity FindOne(IAbstractQuery<TEntity> abstractQuery);

        IEnumerable<TEntity> FindAll(IAbstractQuery<TEntity> abstractQuery);

        IList<TEntity> PagedQuery(IAbstractQuery<TEntity> abstractQuery, int start, int numItems);

        int RecordsFound(IAbstractQuery<TEntity> abstractQuery);

    }
}
