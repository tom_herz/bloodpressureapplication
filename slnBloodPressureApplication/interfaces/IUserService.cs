using System.Collections.Generic;
using BP.Domain;

namespace BP.Interfaces
{
    public interface IUserService
    {
        IList<User> GetAllUsers();
    }
}