﻿using System;
using System.Collections.Generic;
using BP.Domain;
using common;

namespace BP.Interfaces
{
    public interface IReadingService
    {
        void SaveReading(Reading reading);
      
        void UpdateReading(Reading reading);
        
        IList<Reading> ReadingsByDateRange(DateTime startDate, DateTime endDate);
        
        Reading ReadingById(Guid id);
        
        IList<Reading> GetReadingsByDateRangeAndRating(Rating rating, DateTime startDate, DateTime endDate);
        
        IList<Reading> GetMostRecentReadings(int numberToTake);
        
        IList<Reading> GetAllReadings();
    }
}