﻿using System;
using BP.Domain;
using FluentNHibernate.Mapping;
using common;

namespace BP.Tests.Data.Maps
{
    public class ReadingMap : ClassMap<Reading>
    {
        public ReadingMap()
        {
            Table("Reading");
            Not.LazyLoad();
            Id(x => x.Id).GeneratedBy.Guid().Column("Id");

            Map(x => x.ReadingDate).Column("Date");
            Map(x => x.Systolic).Column("Systolic");
            Map(x => x.Diastolic).Column("Diastolic");
            Map(x => x.Rating).CustomType<Rating>().Default("1");

        }

       

    }
}