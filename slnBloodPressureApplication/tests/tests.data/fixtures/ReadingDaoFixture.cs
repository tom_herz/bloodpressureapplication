﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BP.Data;
using BP.Data.queries;
using BP.Domain;
using NUnit.Framework;
using common;

namespace BP.Tests.Data.fixtures
{
    [TestFixture]
    public class ReadingDaoShould : FixtureBase
    {
        protected IReadingDao ReadingDao;
        private List<Reading> _readings = new List<Reading>();
        private int _numberToTake;

        [SetUp]
        public void run_before_each_test()
        {
            Assert.That(ReadingDao, Is.Not.Null);
            Assert.IsFalse(_readings.Any());

        }

        [Test]
        public void satisfy_spring_nh_config()
        {
            var query = new QueryAllReadings();

            var readings = ReadingDao.FindAll(query);

            Assert.That(readings.Any(), Is.False);
        }

        [Test]
        public void update_reading()
        {
            var reading = new Reading
                {
                    ReadingDate = DateTime.Today,
                    Diastolic = 90,
                    Systolic = 140,
                };

            ReadingDao.Add(reading);

            reading.Diastolic = 110;

            ReadingDao.Update(reading);

            var fetchedReading = ReadingDao.GetById(reading.Id);

            Assert.IsNotNull(fetchedReading);
            Assert.That(fetchedReading.Diastolic, Is.EqualTo(110));
        }

        [Test]
        public void get_readings_by_date_range()
        {
            create_test_readings(10);
            saveReadings();

            var startDate = DateTime.Today.AddDays(3);
            var endDate = DateTime.Today.AddDays(6);

            var query = new QueryForReadingsByDateRange()
                {
                    Start = startDate,
                    End = endDate
                };

            var fetchedValues = ReadingDao.FindAll(query) as IList<Reading>;

            Assert.IsNotNull(fetchedValues);
            Assert.That(fetchedValues.Count, Is.EqualTo(4));
            Assert.IsTrue(fetchedValues.Any(x => x.ReadingDate == startDate));
            Assert.IsTrue(fetchedValues.Any(x => x.ReadingDate == endDate));


        }

        [Test]
        public void query_for_readings_by_date_range_and_rating()
        {
            create_test_readings(10);
        _readings.Add(new Reading(){
            
                        ReadingDate = DateTime.Today.AddDays(4),
                        Rating = Rating.Low,
                        Systolic = 100,
                        Diastolic = 60
                    });

            saveReadings();


            var startDate = DateTime.Today.AddDays(3);
            var endDate = DateTime.Today.AddDays(6);

            var query = new QueryForReadingsByRatingAndDateRange()
            {
                Start = startDate,
                End = endDate,
                Rating = Rating.Low
            };

            var fetchedValues = ReadingDao.FindAll(query) as IList<Reading>;

            Assert.IsNotNull(fetchedValues);
            Assert.That(fetchedValues.Count, Is.EqualTo(1));
            Assert.IsTrue(fetchedValues.Any(x => x.ReadingDate == DateTime.Today.AddDays(4)));
            Assert.IsTrue(fetchedValues.Any(x => x.Rating == Rating.Low));
        }
        
        [Test]
        public void assert_that_diastolic_reading_above_90_has_rating_of_high()
        {
            create_test_readings(10);
            saveReadings();

            var fetchedReadings =ReadingDao.FindAll(new QueryForHighRatings());
            Console.WriteLine(fetchedReadings.Count());
            Assert.IsTrue(fetchedReadings.Any());
            Assert.IsTrue(fetchedReadings.Count() == 6);
            
        }

        [Test]
        public void assert_that_diastolic_reading_below_75_has_rating_of_low()
        {
            create_test_readings(10);
            saveReadings();

            var fetchedReadings = ReadingDao.FindAll(new QueryForLowratings());

            Assert.IsTrue(fetchedReadings.Any());
            Assert.IsTrue(fetchedReadings.Count() == 1);

        }
        
        [Test]
        public void assert_that_diastolic_reading_between_76_and_90_is_normal()
        {
            create_test_readings(10);
            saveReadings();

            var fetchedReadings = ReadingDao.FindAll(new QueryNormalReadings());

            Assert.IsTrue(fetchedReadings.Any());
            Assert.IsTrue(fetchedReadings.Count() == 3);

        }

        [Test]
        public void query_most_recent_entries()
        {
            
            create_test_readings(30);
            saveReadings();

            _numberToTake = 3;
            var query = new QueryForRecentReadings()
                {
                    NumberToTake = _numberToTake
                };

            var fetchedRecords = ReadingDao.FindAll(query) as IList<Reading>;

            Assert.IsTrue(fetchedRecords.Any());
            Assert.That(fetchedRecords.Count, Is.EqualTo(_numberToTake));


        }
     
        [TearDown]
        public void run_after_each_test()
        {
            _readings.Clear();
        }

        #region private helpers

        private void create_test_readings(int amount)
        {
            var inc = 0; //used to get the desired reading ratings for the high/low/normal tests

            for (int i = 0; i < amount; i++)
            {
                var reading = new Reading()
                    {
                        ReadingDate = DateTime.Today.AddDays(i),
                        Systolic = 150 + i,
                        Diastolic = 70 +inc
                      
                    };
                Console.WriteLine(String.Format(@"The diastolic reading for reading number: {0} is {1}.  The rating is: {2}", i, reading.Diastolic, reading.Rating));
                _readings.Add(reading);
                
                inc += 6;
            }
        }

        private void saveReadings()
        {
            if (_readings.Any())
            {
                foreach (var reading in _readings)
                {
                    ReadingDao.Add(reading);
                }
            }
       
        }

        #endregion


    }
}
