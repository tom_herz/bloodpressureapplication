﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BP.Data;
using BP.Data.queries;
using NUnit.Framework;

namespace BP.Tests.Data.fixtures
{
    [TestFixture]

    public class WeightDaoShould : FixtureBase
    {
        protected IWeightDao WeightDao;

        [Test]
        public void satisfy_spring_Nhibernate()
        {
            var dao = WeightDao.FindAll(new QueryAllWeightRecords());
            Assert.IsFalse(dao.Any());
        }
    }
}
