﻿using System;
using System.Collections.Generic;
using System.Linq;
using BP.Common;
using BP.Data;
using BP.Data.queries;
using BP.Domain;
using NUnit.Framework;

namespace BP.Tests.Data.fixtures
{
    [TestFixture]
    public class UserDaoShould : FixtureBase
    {
        private const string _firstname = "FirstName";
        private const string _lastname = "LastName";

        protected IUserDao UserDao;
        private IList<User> _users = new List<User>();
        private string _somecompany = "SomeCompany";

        [SetUp]
        public void run_before_test()
        {
            Assert.IsNotNull(UserDao);

            Assert.IsFalse(_users.Any());
        }

        [Test]
        public void satisfy_spring_config()
        {
            var fetchedRecords = UserDao.FindAll(new QueryForAllUsers());

            Assert.IsFalse(fetchedRecords.Any());
        }

        [Test]
        public void get_all_users()
        {
            create_users_to_test(10);
            saveUsers();

            var query = new QueryForAllUsers();

            var fetchedData = UserDao.FindAll(query) as IList<User>;

            Assert.That(fetchedData.Any(), Is.True);
            Assert.That(fetchedData.Count, Is.EqualTo(10));
        }

         [Test]
        public void query_readings_by_user_id()
        {
            create_users_to_test(10);
            saveUsers();

            var query = new QueryAllReadingsByUserId()
            {
                UserId = _users[0].Id
            };


             var fetchedRecords = UserDao.FindOne(query);

            Assert.IsNotNull(fetchedRecords);
             Assert.IsTrue(fetchedRecords.Readings.Any());
             Assert.That(fetchedRecords.Readings.Count, Is.EqualTo(10));


        }

        [TearDown]
        public void run_after_each_test()
        {
            _users.Clear();
        }

        #region private helpers


        private void saveUsers(bool persist = false)
        {
            if (_users.Any())
            {
                foreach (var user in _users)
                {
                    UserDao.Add(user);
                }
}
            if (persist)
                SetComplete();

        }


        private void create_users_to_test(int i)
        {


            for (int j = 0; j < i; j++)
            {
                var user = new User
                    {
                        First = String.Format(@"{0}_{1}", _firstname, j),
                        Last = String.Format(@"{0}_{1}", _lastname, j),
                        ContactInformation = create_contactinformation_to_test(),
                        Readings = create_test_readings(10)
                    };
                _users.Add(user);
            }
        }

        private IList<Reading>  create_test_readings(int amount)
        {
            var inc = 0; //used to get the desired reading ratings for the high/low/normal tests
            IList<Reading> readings = new List<Reading>();

            for (int i = 0; i < amount; i++)
            {
                var reading = new Reading()
                {
                    ReadingDate = DateTime.Today.AddDays(i),
                    Systolic = 150 + i,
                    Diastolic = 70 + inc

                };
                //   Console.WriteLine(String.Format(@"The diastolic reading for reading number: {0} is {1}.  The rating is: {2}", i, reading.Diastolic, reading.Rating));
                readings.Add(reading);

                inc += 6;
            }
            return readings;
        }

        private IList<ContactInformation> create_contactinformation_to_test()
        {
            IList<ContactInformation> contactInformation = new List<ContactInformation>();
            for (int j = 0; j < 5; j++)
            {
                var tmpcontactInfomation = new ContactInformation()
                    {
                        EmailAddress = String.Format(@"{0}_{1}.{2}_{1}@{3}.com", _firstname, j, _lastname, _somecompany),
                        EmailAddressType = (EmailAddressType)j,

                    };
                contactInformation.Add(tmpcontactInfomation);
            }
            return contactInformation;
        }

        #endregion

    }
}
