﻿
using BP.Data;
using BP.Tests.Data;
using BP.Tests.Data;
using FluentNHibernate.Cfg;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;


namespace BP.Tests.Data
{
    [TestFixture, Explicit()]
    public class _MappingsFixture
    {

        private Configuration _configuration;
        private SchemaExport _export;
   

        [SetUp]
        public void run_before_each_test()
        {
            _configuration = Fluently.Configure()
                                     .Database(
                                         FluentNHibernate.Cfg.Db.MsSqlConfiguration.MsSql2008.ConnectionString(
                                             c => c.FromConnectionStringWithKey("Test_Connection")))
                                     .ExposeConfiguration(
                                         c => c.SetProperty(global::NHibernate.Cfg.Environment.Hbm2ddlAuto, "create"))
                                     .Mappings(m => m.FluentMappings.AddFromAssembly(typeof (ReadingMap).Assembly))
                                    
                                     .ExposeConfiguration((cfg => new SchemaUpdate(cfg).Execute(true, true)))
                                     .BuildConfiguration();

            _export = new  SchemaExport(_configuration);
        }




        [Test]
        public void create_schema()
        {
            _export.Execute(true, true, false);
        }

        [Test]
        public void drop_schema()
        {
            _export.Drop(true, true);
        }
    }
}
