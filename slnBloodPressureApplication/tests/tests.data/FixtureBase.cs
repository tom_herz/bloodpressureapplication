﻿using System;
using NHibernate;
using NUnit.Framework;
using Spring.Testing.NUnit;

namespace BP.Tests.Data
{
    [TestFixture]
    public abstract class FixtureBase : AbstractTransactionalDbProviderSpringContextTests
    {

        protected ISessionFactory NHibernateSessionFactory;

        [TestFixtureSetUp]
        public void FixtureStart()
        {

            DefaultRollback = true;

            PopulateProtectedVariables = true;

            AutowireMode = Spring.Objects.Factory.Config.AutoWiringMode.ByName;

        }

        [TestFixtureTearDown]
        public void FixtureEnd()
        {
           SetComplete();
        }

        [SetUp]
        protected virtual void StartTest()
        {
            Assert.That(NHibernateSessionFactory, Is.Not.Null, "NHibernate Session Factory is not initialized.");

        }

        [TearDown]
        protected virtual void EndTest()
        {
            base.EndTransaction();
        }

        protected override string[] ConfigLocations
        {
            get
            {
                return new String[]
                    {
                        "assembly://BP.Tests.Data/BP.Tests.Data.config/NHibernate.xml",
                        "assembly://BP.Resources/BP.Resources/Dao.xml",
                        "assembly://BP.Resources/BP.Resources/Service.xml",
                        "assembly://BP.Resources/BP.Resources/Transactions.xml",
                                             "config://spring/objects"
                    };
            }
        }


    }
}
