﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BP.Domain;
using BP.Interfaces;
using BP.Service;
using Spring.Context;
using Spring.Context.Support;
using System.Configuration;
namespace BloodPressureConsole
{
    class Program 
    {
        protected static IReadingService Reading;
       
        static void Main(string[] args)
        {

            IApplicationContext ctx = new XmlApplicationContext(false, ConfigLocations);

          IReadingService readingService =   (ReadingService) ctx.GetObject ("ReadingService");
            
            bool result = false;

            if (args.Length > 3)
                throw new ArgumentException();
            
            if (args.Length < 3)
                throw new ArgumentException();


            var reading = new Reading()
                {
                    ReadingDate = Convert.ToDateTime(args[0]),
                    Systolic = Convert.ToDouble(args[1]),
                    Diastolic = Convert.ToDouble(args[2])
                };

            try
            {
                readingService.SaveReading(reading);
             
            }
            catch (Exception e)
            {
                throw new ArgumentException();
               
            }

            
        }
        public static string[] ConfigLocations
        {
            get
            {
                return new String[]
                    {
                        "assembly://BP.Resources/BP.Resources.config/NHibernate.xml",
                        "assembly://BP.Resources/BP.Resources/Dao.xml",
                        "assembly://BP.Resources/BP.Resources/Service.xml",
                        "assembly://BP.Resources/BP.Resources/Transactions.xml",
                                             "config://spring/objects"
                    };
            }
        }
    }
}
