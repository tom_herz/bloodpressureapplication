﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using BP.Data;
using BP.Data.queries;
using BP.Domain;
using BP.Interfaces;
using common;


namespace BP.Service
{
    public class ReadingService : IReadingService
    {
        protected IReadingDao ReadingHibernateDao { private get; set; }

        public void SaveReading(Reading reading)
        {
            ReadingHibernateDao.Add(reading);
        }

        public void UpdateReading(Reading reading)
        {
            ReadingHibernateDao.Update(reading);
        }

        public IList<Reading> ReadingsByDateRange(DateTime startDate, DateTime endDate)
        {
            var query = new QueryForReadingsByDateRange()
                {
                    Start = startDate,
                    End = endDate
                };
            return ReadingHibernateDao.FindAll(query) as IList<Reading>;
        }

        public Reading ReadingById(Guid id)
        {
            return ReadingHibernateDao.GetById(id);
        }


        public IList<Reading> GetReadingsByDateRangeAndRating(Rating rating, DateTime startDate, DateTime endDate)
        {
            var query = new QueryForReadingsByRatingAndDateRange()
                {
                    Rating = rating,
                    Start = startDate,
                    End = endDate
                };

            return ReadingHibernateDao.FindAll(query) as List<Reading>;

        }

        public IList<Reading> GetMostRecentReadings(int numberToTake)
        {
            var query = new QueryForRecentReadings()
                {
                    NumberToTake = numberToTake
                };

            return ReadingHibernateDao.FindAll(query) as IList<Reading>;
        }

        public IList<Reading> GetAllReadings()
        {
            return ReadingHibernateDao.FindAll(new QueryAllReadings()) as IList<Reading>;
        }

    }
}