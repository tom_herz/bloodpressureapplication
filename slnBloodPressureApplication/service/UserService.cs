using System.Collections.Generic;
using BP.Data;
using BP.Data.queries;
using BP.Domain;
using BP.Interfaces;

namespace BP.Service
{
    public class UserService : IUserService
    {
        protected IUserDao UserHibernateDao { private get; set; }

        public IList<User> GetAllUsers()
        {
            var query = new QueryForAllUsers();

            return UserHibernateDao.FindAll(query) as IList<User>;
        }
    }
}